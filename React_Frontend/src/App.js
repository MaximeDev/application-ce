import React, { Component } from 'react';
import axios from 'axios';
import Zoom from 'react-reveal/Zoom';
import Flip from 'react-reveal/Flip'

class App extends Component {
  constructor() {
    super();
    this.state = {
      allData: [],
    };
}

clickPost(e){
  e.preventDefault();
  var url = 'http://localhost:3210/data';
  axios.post(url, {
    email: this.inputEmail.value,
    password: this.inputPassword.value
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
};

clickUpdate(e){
  e.preventDefault();
  var url = 'http://localhost:3210/data';
  axios.put(url, {
    email: this.inputEmail.value,
    password: this.inputPassword.value
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
};

clickDelete(e){
  e.preventDefault();
  var url = 'http://localhost:3210/data';
  axios.delete(
    url,
    {headers: {
      Authorization: "123"
    },
    data:{
      email: this.inputEmail.value,
      password: this.inputPassword.value
    }}
  );
};


clickGet(e){
  e.preventDefault();
  var url = 'http://localhost:3210/data';
  axios.get(url)
  .then((getData) => {
    console.log(getData.data);
    this.setState({
      allData: getData.data,
    }) 
  })
};

render() {
  const dataMongo = this.state.allData.map((item, index)=>{
    var tableau = ['email: ',item.email,', password: ', item.password].join(' ');
    return (<p key={index}>{tableau}</p>);
  })
  return (
    <div className="container">
      <Zoom>
        <center style={{margin:'25px'}}>
          <Flip><h3>Backoffice</h3></Flip>
          <form>
            <div className="form-group" style={{margin:'15px'}}>
              <input 
                className="form-control"
                type="text" id="email"
                ref={ inEmail => this.inputEmail = inEmail }
                placeholder="Input email!"
              />
            </div>

            <div className="form-group" style={{margin:'15px'}}>
              <input
                className="form-control"
                type="text" id="password" 
                ref={ inPassword => this.inputPassword = inPassword }
                placeholder="Input password!"/>
            </div>
      
            <button className="btn btn-primary" style={{width:'100px'}}
            onClick={this.clickPost.bind(this)}>POST
            </button>
          
            <button className="btn btn-success" style={{margin:'15px',width:'100px'}}
            onClick={this.clickGet.bind(this)}>GET
            </button>

            <button className="btn btn-primary" style={{width:'100px'}}
            onClick={this.clickDelete.bind(this)}>DELETE
            </button>

          </form>

          <div>
            { dataMongo }
          </div>

        </center>
      </Zoom>
    </div>
  );
 }
 }
 
export default App;