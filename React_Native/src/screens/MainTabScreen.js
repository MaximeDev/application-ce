import React from 'react';

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import { Ionicons } from '@expo/vector-icons';

import HomeScreen from './HomeScreen';
import HomeStackNavigator from './HomeStackNavigator';
import ChatScreen from './ChatScreen';
import InfoScreen from './InfoScreen';
import VisioScreen from './VisioScreen';

const HomeStack = createStackNavigator();
const ChatStack = createStackNavigator();
const VisioStack = createStackNavigator();
const InfoStack = createStackNavigator();

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="#fff"
    >
      <Tab.Screen
        name="Acceuil"
        component={HomeStackScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarColor: '#009387',
          tabBarIcon: ({ color }) => (
            <Ionicons name="ios-home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Chat"
        component={ChatStackScreen}
        options={{
          tabBarLabel: 'Chat',
          tabBarColor: '#009387',
          tabBarIcon: ({ color }) => (
            <Ionicons name="ios-chatboxes" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Visio"
        component={VisioStackScreen}
        options={{
          tabBarLabel: 'Visio',
          tabBarColor: '#009387',
          tabBarIcon: ({ color }) => (
            <Ionicons name="ios-images" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Infos"
        component={InfoStackScreen}
        options={{
          tabBarLabel: 'Infos',
          tabBarColor: '#009387',
          tabBarIcon: ({ color }) => (
            <Ionicons name="ios-megaphone" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
);

export default MainTabScreen;

const HomeStackScreen = ({navigation}) => (
<HomeStack.Navigator screenOptions={{
        headerStyle: {
        backgroundColor: '#009387',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
        fontWeight: 'bold'
        }
    }}>
        <HomeStack.Screen name="Acceuil" component={HomeStackNavigator} options={{
        title:'Acceuil',
        headerLeft: () => (
            <Ionicons.Button 
              name="ios-menu" 
              size={25} 
              backgroundColor="#009387" 
              onPress={() => navigation.openDrawer()}>
            </Ionicons.Button>
        )
        }} />
</HomeStack.Navigator>
);

const ChatStackScreen = ({navigation}) => (
<ChatStack.Navigator screenOptions={{
        headerStyle: {
          backgroundColor: '#009387',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
          fontWeight: 'bold'
          }
      }}>
        <ChatStack.Screen name="Chat" component={ChatScreen} options={{
          title:'Chat',
        headerLeft: () => (
            <Ionicons.Button
              name="ios-menu"
              size={25}
              backgroundColor="#009387"
              onPress={() => navigation.openDrawer()}>
            </Ionicons.Button>
        )
        }} />
</ChatStack.Navigator>
);

const VisioStackScreen = ({navigation}) => (
  <VisioStack.Navigator screenOptions={{
          headerStyle: {
            backgroundColor: '#009387',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
            fontWeight: 'bold'
            }
        }}>
          <VisioStack.Screen name="Detailles" component={VisioScreen} options={{
            title:'Visio',
          headerLeft: () => (
              <Ionicons.Button
                name="ios-menu"
                size={25}
                backgroundColor="#009387"
                onPress={() => navigation.openDrawer()}>
              </Ionicons.Button>
          )
          }} />
  </VisioStack.Navigator>
  );

  const InfoStackScreen = ({navigation}) => (
    <InfoStack.Navigator screenOptions={{
            headerStyle: {
              backgroundColor: '#009387',
              },
              headerTintColor: '#fff',
              headerTitleStyle: {
              fontWeight: 'bold'
              }
          }}>
            <InfoStack.Screen name="Detailles" component={InfoScreen} options={{
              title:'Info',
            headerLeft: () => (
                <Ionicons.Button
                  name="ios-menu"
                  size={25}
                  backgroundColor="#009387"
                  onPress={() => navigation.openDrawer()}>
                </Ionicons.Button>
            )
            }} />
    </InfoStack.Navigator>
    );