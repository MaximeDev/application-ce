import React, { useState, useEffect } from 'react';
import { 
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground
 } from 'react-native';
import axios from 'axios';
import Swiper from "react-native-swiper"
import { LinearGradient } from 'expo-linear-gradient';


const baseUrl = 'https://api.conseilce.fr/pac/articles/index.php/7/all';

const HomeDetailsScreen = ({
    navigation,
    id,
    }) => {  
  
  const [userRequest, setUserRequest] = useState([]);
  const { 
    data,
  } = userRequest;

    useEffect(() => {
        axios.get(baseUrl)
            .then(({ data }) => {
              setUserRequest({
                data
              })
            })
            .catch((error) => console.error(error));
    }, []);

   const _renderItem = ({ item }) => (
      <View style={[styles.divnews,styles.shadows]}>
        <Image style={styles.imagenew} source={{uri : item.img}} />
        <View style={{padding:5}}>
          <Text style={styles.titleNews} numberOfLines={2} >
            {item.titre}
          </Text>
          <Text style={styles.themeNews}> {item.type} </Text>
          <Text> {item.date_creation}</Text>
        </View>
      </View>
    )
    
    return (
      <View style={{ flex: 1, backgroundColor:'#f8f8f8'}}>
      <View style={styles.headernews}>
          <Image style={styles.logonews} source={ require("../../assets/logo.png")} />
      </View>
      <Text>{id}</Text>
      <FlatList
        data={data}
        renderItem={_renderItem}
        keyExtractor={item => item.id}
      />
      </View>
    );
};

export default HomeDetailsScreen;

var {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  imagenew:{
    width:width/3,
    height:width/3,
    resizeMode:'cover',
    borderRadius:5
  },
  shadows:{
    elevation:4,
    shadowOpacity:0.3,
    shadowRadius:50,
    shadowColor:'gray',
    shadowOffset: { height:0, width:0 }
  },
  divnews:{
    width:width-10,
    backgroundColor:'white',
    margin:5,
    flexDirection:'row',
    borderRadius:5
  },
  titleNews:{
    width:((width/3)*2)-20,
    fontSize:22
  },
  themeNews:{
    color:"#c2191c",
    fontSize:20
  },
  headernews:{
    width:width,
    height:50,
    backgroundColor:"#f8f8f8",
    alignItems:'center',
    justifyContent:'center'
  },
  logonews:{
    height:45,
    width:width/3,
    resizeMode:'contain'
  },
  textBanner:{
    fontSize:25,
    color:'white'
  },
  fondoBanner:{
    flex:2,
    justifyContent:"flex-end",
    padding:10
  },
  divtheme:{
    height:42,
    borderTopWidth:3,
    padding:10,
    borderColor:'#c2191c',
    backgroundColor:'white'
  },
  divtheme2:{
    height:41,
    borderBottomWidth:2,
    borderColor:'#c2191c',
    padding:10,
    backgroundColor:'#343434'
  },
  textTheme:{
    color:'black'
  },
  textTheme2:{
    color:'white'
  }
})