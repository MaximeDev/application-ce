import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from "./HomeScreen"
import HomeDetailsScreen from "./HomeDetailsScreen"
import SplashScreen from './SplashScreen';

const HomeStack = createStackNavigator();

const HomeStackNavigator = ({navigation}) => (
    <HomeStack.Navigator headerMode='none'>
        <HomeStack.Screen name="HomeScreen" component={HomeScreen}/>
        <HomeStack.Screen name="HomeDetailsScreen" component={HomeDetailsScreen}/>
        <HomeStack.Screen name="SplashScreen" component={SplashScreen}/>
    </HomeStack.Navigator>
);

export default HomeStackNavigator;