import React, { useState, useEffect } from 'react';
import { 
  ScrollView,
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ImageBackground
 } from 'react-native';
import axios from 'axios';
import Swiper from "react-native-swiper"
import { LinearGradient } from 'expo-linear-gradient';

const baseUrl = 'https://api.conseilce.fr/pac/articles/index.php/7/all';

const HomeScreen = ({navigation}) => {  
  
  const [data, setData] = useState([]);
  const [dataBanner, setdataBanner] = useState([]);
  const [dataBackup, setdataBackup] = useState([]);
  const [dataAllNews, setdataAllNews] = useState({});
  const [selectTheme, setTheme] = useState(0);
  const textSearch = "";
  
  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(baseUrl);
      setdataBanner(result.data);
      setData(result.data);
      setdataBackup(result.data);
      setdataAllNews(result.data);
    };
    fetchData();
  }, []);

  const onChangeSearch = ({text}) => {
    console.log(text)
    const data = dataBackup
    const newData = data.filter(function(item){
        const itemData = item.title.toUpperCase()
        const textData = text.toUpperCase()
        return itemData.indexOf(textData) > -1
    })
    console.log(newData.length)
    setState({
        data: newData,
        textSearch: text,
    })
  }

   const _renderItem = ({ item }) => (
     <TouchableOpacity
     onPress={()=>navigation.navigate('HomeDetailsScreen')}>
      <View style={[styles.divnews,styles.shadows]}>
        <Image style={styles.imagenew} source={{uri : item.img}} />
        <View style={{padding:5}}>
          <Text style={styles.titleNews} numberOfLines={2} >
            {item.titre}
          </Text>
          <Text style={styles.themeNews}> {item.type} </Text>
          <Text> {item.date_creation}</Text>
        </View>
      </View>
      </TouchableOpacity>
    )

    _renderItemTheme = (item) => (
      <TouchableOpacity onPress={()=>setTheme({selectTheme:item.id}) }>
        <View style={selectTheme==item.id?styles.divtheme:styles.divtheme2}>
          <Text style={selectTheme==item.id?styles.textTheme:styles.textTheme2}> categorie </Text>
        </View>
      </TouchableOpacity>
    )
    
    return (
      <ScrollView>
      <View style={{ flex: 1, backgroundColor:'#f8f8f8'}}>
      <View style={{width:width, backgroundColor:'gray',padding:8}}>
            <TextInput
              placeholder="Search..."
              style={{ height: 40, borderColor: '#f2f2f2', borderRadius:10, backgroundColor:'white', borderWidth: 1, paddingHorizontal:20 }}
              onChangeText={(text) => this.onChangeSearch(text)}
              value={textSearch}
            />
          </View>
      <View style={{height:200}}>
          <Swiper>
            {dataBanner.map(itemimag => (
              <ImageBackground style={{width:width,height:200}} source={{uri:  itemimag.img }}>
                <LinearGradient style={styles.fondoBanner} colors={[ 'transparent', 'black']} >
                  <Text style={styles.textBanner} numberOfLines={2}>{itemimag.titre}</Text>
                </LinearGradient>
              </ImageBackground>
            ))}
          </Swiper>
      </View>
      <View style={{height:45}}>
        <FlatList
          horizontal={true}
          data={data}
          keyExtractor={(item, index) => item.id}
          renderItem={({item})=>_renderItemTheme(item)}
          />
        </View>
      <FlatList
        data={data}
        renderItem={_renderItem}
        keyExtractor={item => item.id}
      />
      </View>
      </ScrollView>
    );
};

export default HomeScreen;

var {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  imagenew:{
    width:width/3,
    height:width/3,
    resizeMode:'cover',
    borderRadius:5
  },
  shadows:{
    elevation:4,
    shadowOpacity:0.3,
    shadowRadius:50,
    shadowColor:'gray',
    shadowOffset: { height:0, width:0 }
  },
  divnews:{
    width:width-10,
    backgroundColor:'white',
    margin:5,
    flexDirection:'row',
    borderRadius:5
  },
  titleNews:{
    width:((width/3)*2)-20,
    fontSize:22
  },
  themeNews:{
    color:"#c2191c",
    fontSize:20
  },
  headernews:{
    width:width,
    height:50,
    backgroundColor:"#f8f8f8",
    alignItems:'center',
    justifyContent:'center'
  },
  logonews:{
    height:45,
    width:width/3,
    resizeMode:'contain'
  },
  textBanner:{
    fontSize:25,
    color:'white'
  },
  fondoBanner:{
    flex:2,
    justifyContent:"flex-end",
    padding:10
  },
  divtheme:{
    height:42,
    borderTopWidth:3,
    padding:10,
    borderColor:'#c2191c',
    backgroundColor:'white'
  },
  divtheme2:{
    height:41,
    borderBottomWidth:2,
    borderColor:'#c2191c',
    padding:10,
    backgroundColor:'#343434'
  },
  textTheme:{
    color:'black'
  },
  textTheme2:{
    color:'white'
  }
})