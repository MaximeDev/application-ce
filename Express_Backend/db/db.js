const mongoose = require("mongoose");

const url = 'mongodb://admin:admin@localhost:27017/myDatabase';

mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });

module.exports = { mongoose };
