import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text } from 'react-native';
import axios from 'axios';

import PostTile from './PostTile';

const ENDPOINT_DATA = 'https://api.conseilce.fr/pac/articles/index.php/7/all';

const PostList = ({
}) => {
    const [data, setData] = useState([]);
    // Je récupère les données grâce au module axios et au Hooks useEffect.
    useEffect(() => {
        axios.get(ENDPOINT_DATA)
            .then(({ data }) => {
                setData(data)
            })
            .catch((error) => console.error(error));
    }, []);
    /*je créer une fonction "_renderItem" qui me redirige vers un 
    composant creer "PostTile" pour pouvoir afficher les données en lui 
    passant les paramètres hérités de Flatlist grâce à "data".*/
    const _renderItem = ({ item }) =>
        <PostTile
            id={item.id}
            title={item.titre}
            description_short={item.description_short}
            content={item.contenu}
            date={item.date_creation}
            img={item.img}
        ></PostTile>
    return (
        /* Ensuite grâce à Flatlist (Intégrer dans react native) 
        vais pouvoir afficher mes données, en passant pour le paramètre 
        "data" les données récupéré par axios depuis l'url. */
        < FlatList
            data={data}
            renderItem={_renderItem}
            keyExtractor={item => item.id}
        />
    );
};

export default PostList;

