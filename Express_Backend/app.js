var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors');
app.use(cors());
app.use(bodyParser.json());
var mongo = require('mongodb').MongoClient;
var url = 'mongodb://admin:admin@localhost:27017/myDatabase';

mongo.connect(url, (err)=>{
    console.log('Connecter à myDatabase!')
})

app.get('/data', (req, res)=>{
    mongo.connect(url, (err, db)=>{
        var collection = db.collection('myCollection');
        collection.find({}).toArray((x, result)=>{
            res.send(result);
        })
    })
})

app.delete('/data', (req, res)=>{
    mongo.connect(url, function(err, db) {
        var collection = db.collection('myCollection');
        var something = {
            email: req.body.email,
            password: req.body.password
        }
        collection.deleteOne(something, (x, result)=>{
            res.send(result);
        })
    })
})

app.post('/data', (req, res)=>{
    mongo.connect(url, (err, db)=>{
        var collection = db.collection('myCollection');
        var something = {
            email: req.body.email,
            password: req.body.password
        }
        collection.insert(something, (x, result)=>{
            res.send(result);
        })
    })
})

app.put('/data', (req, res) => {
    mongo.connect(url, (err, db)=>{
        var collection = db.collection('myCollection');
        var something = {
            email: req.body.email,
            password: req.body.password
        }
        collection.findOneAndUpdate(something,
            {
              $set: {
                email: "success",
                password: "success"
              }
            },
            (x, result)=>{
            res.send(result);
        })
    })
})



app.listen(3210, ()=>{
    console.log('Server actif @port 3210!');
})