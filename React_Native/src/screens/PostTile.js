import React from 'react';
import { Image, Text, View, StyleSheet, TouchableOpacity, Linking } from 'react-native';
import HTML from 'react-native-render-html';


const PostTile = ({
    title,
    description_short,
    content,
    date,
    img,
}) => {
    const ENPOINT_URL = ""
    const SRC = new RegExp(/src=\"\/(.*?)\"/g)
    var content = content.replace(SRC, ENPOINT_URL);

    return (
        <View style={styles.container}>
            <View style={styles.subcontainer}>
                <Image
                    style={styles.icon}
                    source={{ uri: img }} />
                <Text style={{ color: 'blue' }}
                    onPress={() => Linking.openURL(img)}>
                    <Text style={styles.title}>titre : {title}</Text>
                </Text>
                <Text style={styles.title}>date : {date}</Text>
                <Text style={styles.title}>desc : {description_short}</Text>
                <HTML style={styles.title} html={content} onLinkPress={(evt, href) => { Linking.openURL(href); }} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    subContainer: {
        flexDirection: 'row',
        // justifyContent: 'flex-start',
        alignItems: 'center',
    },
    title: {
        marginLeft: 30
    },
    icon: {
        width: 350,
        height: 250,
        margin: 5,
    }
})

export default PostTile;
